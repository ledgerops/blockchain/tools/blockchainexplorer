### Ethereum docker based testnet


##### Start network
```
$ docker-compose up -d
```

##### Stop network

```
$ docker-compose down
```

NOTE: The rpc port is 8545.